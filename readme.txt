EVE-NG directory contains files needed to install it.
	ISO file is to be used when installing EVE-NG as a host OS.
	OVA file along with two VDMK disk files can be used to deploy EVE-NG as a VM. 
	This VM has all necessary laboratories along with device software images prepared.
	VDMK disk files had to be split into multiple files due to it's size,
	to put them back together run following commands.
		cat disk-1_split_? > disk-1.vdmk
		cat disk-2_split_? > disk-2.vdmk

	Exported_labs subdirectory contains minimal and full SD-WAN EVE-NG topologies, that can be imported into fresh install of EVE-NG.
	Software_images subdirectory contains prepared directories of used devices. To use these in a fresh EVE-NG install simply copy them to /opt/unetlab/addons/qemu
Running_configurations directory contains running-configuration text files of all of the components used in SD-WAN_full EVE_NG laboratory. Only to be used for troubleshooting potential mistakes.